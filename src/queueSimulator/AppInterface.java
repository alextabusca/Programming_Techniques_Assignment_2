package queueSimulator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

// GUI Interface for the Queue Simulator

public class AppInterface {
    JFrame frame = new JFrame("Client Queue Simulator");
    JPanel queuePanel = new JPanel();
    JPanel input = new JPanel();

    JTextField text_simInterval = new JTextField();
    JTextField text_minArrival = new JTextField();
    JTextField text_maxArrival = new JTextField();
    JTextField text_minService = new JTextField();
    JTextField text_maxService = new JTextField();
    JTextField text_maxNumberOfQueues = new JTextField();

    JButton startButton = new JButton("Start Simulation");
    JLabel labelCurrentTime = new JLabel("0");

    public AppInterface() {

        frame.setSize(1000, 650);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setLayout(new BorderLayout());

        input.setLayout(new GridLayout(1, 6)); // modify this to place it on the
        // side

        JPanel p1 = new JPanel();
        p1.setLayout(new GridLayout(2, 1));
        p1.add(new JLabel("Sim interval:"));
        p1.add(text_simInterval);
        input.add(p1);

        JPanel p2 = new JPanel();
        p2.setLayout(new GridLayout(2, 1));
        p2.add(new JLabel("Max Number of Queues"));
        p2.add(text_maxNumberOfQueues);
        input.add(p2);

        JPanel p3 = new JPanel();
        p3.setLayout(new GridLayout(2, 1));
        p3.add(new JLabel("Min Arrival Interval:"));
        p3.add(text_minArrival);
        input.add(p3);

        JPanel p4 = new JPanel();
        p4.setLayout(new GridLayout(2, 1));
        p4.add(new JLabel("Max Arrival Interval:"));
        p4.add(text_maxArrival);
        input.add(p4);

        JPanel p5 = new JPanel();
        p5.setLayout(new GridLayout(2, 1));
        p5.add(new JLabel("Min Service Time:"));
        p5.add(text_minService);
        input.add(p5);

        JPanel p6 = new JPanel();
        p6.setLayout(new GridLayout(2, 1));
        p6.add(new JLabel("Max Service Time:"));
        p6.add(text_maxService);
        input.add(p6);

        JPanel p7 = new JPanel();
        p7.setLayout(new GridLayout(2, 1));
        labelCurrentTime.setHorizontalAlignment(SwingConstants.CENTER);
        labelCurrentTime.setVerticalAlignment(SwingConstants.CENTER);
        p7.add(labelCurrentTime);
        p7.add(startButton);
        input.add(p7);

        input.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3, true));
        frame.add(input, BorderLayout.NORTH);

        queuePanel.setLayout(new GridLayout(1, 3));
        frame.add(queuePanel, BorderLayout.CENTER);

    }

    public void setStartButtonListener(ActionListener a) {
        startButton.addActionListener(a);

    }

    public void displayInformation(ArrayList<MultiThread> queueThreads) {
        queuePanel.removeAll();
        queuePanel.revalidate();

        for (MultiThread t : queueThreads) {
            JPanel qPanel = new JPanel();
            qPanel.setLayout(new BorderLayout());
            if (t.getQueue().getCurrentClient() != null)
                qPanel.add(new JLabel("Current " + t.getQueue().getCurrentClient().toString()), BorderLayout.NORTH);

            qPanel.add(new JLabel("Waiting time: " + t.getQueue().getWaitingTime()), BorderLayout.SOUTH);

            if (!t.getQueue().isEmpty()) {
                JList<Client> listClients = new JList<Client>(t.getQueue().getClients());
                JScrollPane sp = new JScrollPane(listClients);
                qPanel.add(sp, BorderLayout.CENTER);
            } else {
                if (t.getQueue().getCurrentClient() == null)
                    qPanel.add(new JLabel("The queue is empty."));
            }

            queuePanel.add(qPanel);
        }

        queuePanel.repaint();
        queuePanel.revalidate();
    }

    public void setCurrentTime(int currentTime) {
        // TODO Auto-generated method stub
        labelCurrentTime.setText(String.valueOf(currentTime));
    }

    public void showFinalInfo() {
        // TODO Auto-generated method stub
        JOptionPane.showMessageDialog(frame,
                "Min Waiting Time = " + Statistics.getMinimumWaitingTime() + " ; \nMax Waiting Time = "
                        + Statistics.getMaximumWaitingTime() + " ; \nAvg Waiting Time = "
                        + Statistics.getAverageWaitingTime() + " ; \nPeak Hour Start Time =  "
                        + Statistics.getPeakStart() + " ; \nPeak Hour Client Number = " + Statistics.getPeakClients(),
                "Final Statistics", JOptionPane.INFORMATION_MESSAGE);

    }

    public String getSimulationTime() {
        // TODO Auto-generated method stub
        return text_simInterval.getText();
    }

    public void displayError(String string) {
        // TODO Auto-generated method stub
        JOptionPane.showMessageDialog(frame, string + " is not in the required format.", "Input Format Error",
                JOptionPane.ERROR_MESSAGE);
    }

    public String getMaxNumberOfQueues() {
        // TODO Auto-generated method stub
        return text_maxNumberOfQueues.getText();
    }

    public String getMaxArrivalInterval() {
        // TODO Auto-generated method stub
        return text_maxArrival.getText();
    }

    public String getMinArrivalInterval() {
        // TODO Auto-generated method stub
        return text_minArrival.getText();
    }

    public String getMinServiceTime() {
        // TODO Auto-generated method stub
        return text_minService.getText();
    }

    public String getMaxServiceTime() {
        // TODO Auto-generated method stub
        return text_maxService.getText();
    }

}
