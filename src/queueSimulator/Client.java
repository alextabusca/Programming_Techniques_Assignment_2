package queueSimulator;

public class Client {

    int arrivalTime;
    int serviceTime;
    int finishTime = 0;

    // Constructor of the Client class
    public Client(int arrivalTime, int serviceTime) {
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    // Gets the arrival time of the current client
    public int getArrivalTime() {
        return arrivalTime;
    }

    // Sets the arrival time of the current client
    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    // Gets the service time of the current client
    public int getServiceTime() {
        return serviceTime;
    }

    // Sets the service time of the current
    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    // Gets the finish time of the current client
    public int getFinishTime() {
        return finishTime;
    }

    // Sets the finish time of the current
    public void setFinishTime(int finishTime) {
        this.finishTime = finishTime;
    }

    // toString method used for displaying the arrival and service time of the
    // client
    public String toString() {
        if (finishTime == 0)
            return "Arrived: " + String.valueOf(arrivalTime) + " Service Time: " + String.valueOf(serviceTime);
        else
            return "Arrived: " + String.valueOf(arrivalTime) + " Service Time: " + String.valueOf(serviceTime)
                    + " Finished: " + String.valueOf(finishTime);
    }
}
