package queueSimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Logic implements Runnable {

    public static final int SUNIT = 100;

    int simInterval = 100;
    int minService = 1;
    int maxService = 5;
    int minArrival = 1;
    int maxArrival = 5;
    int maxNumberOfQueues = 3;

    private ArrayList<MultiThread> queueThreads;
    private AppInterface gui;
    AtomicInteger currentTime = new AtomicInteger(0);

    public Logic() {
        queueThreads = new ArrayList<MultiThread>();
        queueThreads.add(new MultiThread());
        gui = new AppInterface();
        gui.setStartButtonListener(new StartButtonListener());
    }

    @Override
    public void run() {
        // currentTime = 0
        int nextArrival = 1;
        while (getCurrentTime() < simInterval) {
            currentTime.incrementAndGet();
            gui.setCurrentTime(getCurrentTime());

            boolean newThread = false;
            for (MultiThread t : queueThreads) {
                t.synchronizeCurrentTime(getCurrentTime());
                if (t.getQueue().isOverloaded())
                    newThread = true;
            }
            if (newThread && maxNumberOfQueues > queueThreads.size()) {
                queueThreads.add(new MultiThread());
                redistributeQueues();
            }

            if (getCurrentTime() == nextArrival) {
                MultiThread smallest = queueThreads.get(getSmallestQueueThread());
                int serviceTime = (int) (Math.random() * (maxService - minService) + minService);

                Client c = new Client(getCurrentTime(), serviceTime);
                System.out.println(c.toString());
                smallest.addClientInQueue(c);

                nextArrival = (int) (Math.random() * (maxArrival - minArrival) + getCurrentTime() + minArrival);
            }

            gui.displayInformation(queueThreads);

            try {
                Thread.sleep(Logic.SUNIT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // still run
        // currentTIme > simulationTime
        // need to take care of the clients who are in the queue
        boolean notEmpty = true;
        while (notEmpty) {
            currentTime.incrementAndGet();

            notEmpty = false;
            for (MultiThread t : queueThreads) {
                t.synchronizeCurrentTime(getCurrentTime());
                if (!t.getQueue().isEmpty() || t.getQueue().getCurrentClient() != null)
                    notEmpty = true;
            }

            gui.setCurrentTime(getCurrentTime());
            gui.displayInformation(queueThreads);

            try {
                Thread.sleep(Logic.SUNIT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // System.out.println("troll");
        Statistics.setSimulationFinish(getCurrentTime());
        Statistics.displayClientQueue();
        System.out.println(Statistics.getMinimumWaitingTime());
        System.out.println(Statistics.getMaximumWaitingTime());
        System.out.println(Statistics.getAverageWaitingTime());
        System.out.println(Statistics.getPeakStart());
        System.out.println(Statistics.getPeakClients());

        System.out.println("\nFinal Statistics\n\nMin Waiting Time = " + Statistics.getMinimumWaitingTime()
                + " ; \nMax Waiting Time = " + Statistics.getMaximumWaitingTime() + " ; \nAvg Waiting Time = "
                + Statistics.getAverageWaitingTime() + " ; \nPeak Hour Start Time =  " + Statistics.getPeakStart()
                + " ; \nPeak Hour Clients Number = " + Statistics.getPeakClients());

        gui.showFinalInfo();

        for (MultiThread t : queueThreads) {
            t.terminate();
        }

    }

    /**
     * Gets the smallest queue
     *
     * @return index of the queue which has the lowest waiting time
     */
    private int getSmallestQueueThread() {
        int minWaitTime = queueThreads.get(0).getQueue().getWaitingTime();
        int i = 0;
        for (MultiThread t : queueThreads) {
            if (t.getQueue().getWaitingTime() < minWaitTime)
                i = queueThreads.indexOf(t);
        }

        return i;
    }

    /**
     * After opening a new queue the clients who are the last in their
     * respective queues move to the new empty queue so that they will be evenly
     * distributed across the queues.
     */
    private void redistributeQueues() {
        int size = 0;
        for (MultiThread t : queueThreads)
            size += t.getQueue().getSize(); // compute the total length of the
        // queues
        int updatedSize = size / queueThreads.size();
        System.out.println("New size is " + updatedSize);

        // go on each queue and remove clients from the original queue and put
        // them in another queue as long as that queue is bigger than the
        // new established size
        for (int i = 0; i < queueThreads.size() - 1; i++) {
            Client[] clients = queueThreads.get(i).getQueue().getClients();
            int j = 1;
            while (queueThreads.get(i).getQueue().getSize() > updatedSize) {
                System.out.println("Removing client " + j + "   " + clients[clients.length - j].toString());
                queueThreads.get(queueThreads.size() - 1).addClientInQueue(clients[clients.length - j]);
                queueThreads.get(i).getQueue().removeClient(clients[clients.length - j]);
                j++;
            }
        }
    }

    /**
     * Verifies if the input data is in the correct format
     *
     * @param input
     *            String to be validated.
     * @return True if input is a valid positive non-zero integer, false
     *         otherwise.
     */
    public boolean verifyInput(String input) {
        if (input.equals(""))
            return true;
        return input.matches("^[1-9]\\d*"); // info: cannot start with 0, one or
        // more digits are acceptable
    }

    /**
     * Gets the value of the current simulator time.
     *
     * @return Value of the current time.
     */
    public int getCurrentTime() {
        return currentTime.get();
    }

    private class StartButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            if (verifyInput(gui.getSimulationTime()))
                simInterval = Integer.valueOf(gui.getSimulationTime());
            else {
                gui.displayError("Simulation Interval");
            }

            if (verifyInput(gui.getMaxNumberOfQueues())) {
                maxNumberOfQueues = Integer.valueOf(gui.getMaxNumberOfQueues());
            } else {
                gui.displayError("Max Number of Queues");
            }

            if (verifyInput(gui.getMaxArrivalInterval())) {
                maxArrival = Integer.valueOf(gui.getMaxArrivalInterval());
            } else {
                gui.displayError("Max Arrival Interval");
            }

            if (verifyInput(gui.getMinArrivalInterval())) {
                minArrival = Integer.valueOf(gui.getMinArrivalInterval());
            } else {
                gui.displayError("Min Arrival Interval");
            }

            if (verifyInput(gui.getMinServiceTime())) {
                minService = Integer.valueOf(gui.getMinServiceTime());
            } else {
                gui.displayError("Min Service Time");
            }

            if (verifyInput(gui.getMaxServiceTime())) {
                maxService = Integer.valueOf(gui.getMaxServiceTime());
            } else {
                gui.displayError("Max Service Time");
            }

            Thread th = new Thread(Logic.this);
            th.start();
        }

    }

}
