package queueSimulator;

/**
 * @author Alex The implementation of the class that contains the thread for
 *         each queue. It also serves as a gate between the logic of the
 *         application and the queues
 */

public class MultiThread {
    private Queue queue;
    private Thread th;

    /**
     * Instantiate a multi-thread object and starts the thread associated to it
     */
    public MultiThread() {
        queue = new Queue();
        th = new Thread(queue);
        th.start();
    }

    /**
     * Method to add a client in the queue
     *
     * @param c
     */
    public void addClientInQueue(Client c) {
        queue.addClient(c);
    }

    /**
     * Returns the queue associated to the thread
     *
     * @return
     */
    public Queue getQueue() {
        return queue;
    }

    /**
     * Synchronizes the current time variable of the Logic with the one of the
     * Queue
     *
     * @param currentTime
     */
    public void synchronizeCurrentTime(int currentTime) {
        queue.syncCurrentTime(currentTime);
    }

    /**
     * Interrupts the thread on which the respective queue is run on
     */
    public void terminate() {
        th.interrupt();
    }

}
