package queueSimulator;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue implements Runnable {

    private Client currentClient;
    private AtomicInteger waitTime;
    private AtomicInteger currentTime;
    private BlockingQueue<Client> clientQueue;
    private static final int OVERLOAD_TRESHOLD = 15;

    public Queue() {
        waitTime = new AtomicInteger(0);
        currentTime = new AtomicInteger(0);
        clientQueue = new LinkedBlockingDeque<>();
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (clientQueue.isEmpty())
                    currentClient = null;
                currentClient = clientQueue.take();

                Thread.sleep(currentClient.getServiceTime() * Logic.SUNIT);
                waitTime.addAndGet((-1) * currentClient.getServiceTime());

                if (currentClient != null) {
                    currentClient.setFinishTime(currentTime.get());
                    Statistics.clientQueue.add(currentClient);
                    if (clientQueue.isEmpty())
                        currentClient = null;
                }

            } catch (InterruptedException e) {
                System.out.println("Thread finished.");
            }

        }

    }

    // Adds a client to the queue
    public void addClient(Client c) {
        clientQueue.add(c);
        waitTime.addAndGet(c.getServiceTime());

    }

    // Removes a given client from the queue.
    public void removeClient(Client c) {
        clientQueue.remove(c);
        waitTime.addAndGet((-1) * c.getServiceTime());
    }

    // Gets the current time and synchronizes the value of the time of the
    // thread with this value
    public void syncCurrentTime(int currentTime) {
        this.currentTime.set(currentTime);
    }

    // Gets all the clients in the form of an array form the queue
    public Client[] getClients() {

        Client[] client = new Client[clientQueue.size()];
        clientQueue.toArray(client);

        return client;
    }

    // Gets the client which is currently being processed / serviced
    public Client getCurrentClient() {
        return this.currentClient;
    }

    // Verifies if the queue is empty
    public boolean isEmpty() {
        return clientQueue.isEmpty();
    }

    // Gets the total waiting time of the queue
    public int getWaitingTime() {
        return waitTime.get();
    }

    // Gets the size of the queue
    public int getSize() {
        return clientQueue.size();
    }

    public boolean isOverloaded() {
        return clientQueue.size() > OVERLOAD_TRESHOLD;
    }
}
