package queueSimulator;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

// List of terminated tasks, final informations like average waiting time, peak hour for the simulation interval

public class Statistics {

    public static BlockingQueue<Client> clientQueue = new LinkedBlockingQueue<>();
    public static int simulationFinish = 0;

    public static void displayClientQueue() {
        System.out.println("Client Informations");
        for (Client c : clientQueue) {
            System.out.println(c.toString());
        }
    }

    /**
     * Returns the number of clients that have been processed during the peak
     * hour
     *
     * @return number of clients
     */
    public static int getPeakClients() {
        int peakHour = 0;
        int numberOfClients = 0;
        Client[] client = new Client[clientQueue.size()];
        clientQueue.toArray(client);
        for (int i = 0; i < simulationFinish; i = i + 60) {
            int j = 0;
            int k = 0;
            while (client[j].arrivalTime < i && j < client.length - 1)
                j++;
            while (client[j].finishTime < i + 60 && j < client.length - 1) {
                k++;
                j++;
            }

            if (k > numberOfClients) {
                numberOfClients = k;
                peakHour = i;
            }
        }

        return numberOfClients;
    }

    /**
     * Returns the start time of the peak hour
     *
     * @return starting time
     */
    public static int getPeakStart() {
        int peakHour = 0;
        int numberOfClients = 0;
        Client[] client = new Client[clientQueue.size()];
        clientQueue.toArray(client);
        for (int i = 0; i < simulationFinish; i = i + 60) {
            int j = 0;
            int k = 0;
            while (client[j].arrivalTime < i && j < client.length - 1)
                j++;
            while (client[j].finishTime < i + 60 && j < client.length - 1) {
                k++;
                j++;
            }

            if (k > numberOfClients) {
                numberOfClients = k;
                peakHour = i;
            }
        }

        return peakHour;
    }

    public static void setSimulationFinish(int simulationFinish) {
        Statistics.simulationFinish = simulationFinish;
    }

    public static int getSimulationFinish() {
        return simulationFinish;
    }

    /**
     * Computes the average waiting time for all the clients
     *
     * @return The average waiting time
     */
    public static float getAverageWaitingTime() {
        float waitingTime = 0;
        for (Client c : clientQueue)
            waitingTime = waitingTime + (c.getFinishTime() - c.getArrivalTime());

        return waitingTime / clientQueue.size();
    }

    /**
     * Computes the maximum waiting time for all the clients
     *
     * @return The maximum waiting time
     */
    public static float getMaximumWaitingTime() {
        int waitingTime = 0;
        for (Client c : clientQueue) {
            int temp = c.getFinishTime() - c.getArrivalTime();
            if (temp > waitingTime)
                waitingTime = temp;
        }
        return waitingTime;
    }

    /**
     * Computes the minimum waiting time for all the clients
     *
     * @return The minimum waiting time
     */
    public static float getMinimumWaitingTime() {
        int waitingTime = clientQueue.peek().getFinishTime() - clientQueue.peek().getArrivalTime();
        for (Client c : clientQueue) {
            int temp = c.getFinishTime() - c.getArrivalTime();
            if (temp < waitingTime)
                waitingTime = temp;
        }
        return waitingTime;
    }
}
